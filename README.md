# Ripperoni Pizza
Web Applications, A.Y. 2019/2020
Master Degree in ICT for Internet And Multimedia

# Members
  - Favaro Simone
  - Foscarin Daniele
  - Molon Alberto
  - Neri Michael
  - Shenaj Donald

# Build
mvn clean package javadoc:javadoc

# Login credentials
| Username        | Password         |
|-----------------|------------------|
| NicolasCage     | Nicola76!        |
| SheldonCooper   | Sheldon98!       |
| Admin           | asmdisn43!       |
| LuigiVerdura    | Vegetables87!    |
| Spiderman       | Spiede89@        |
| MarioMortadella | @lol@8475        |
| PinoPastinaca   | Pinuzzu87@?      |
| ElioCarlesso    | ElioEleStorie56_ |